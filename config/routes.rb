Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
	root 'welcome#index'
  	devise_for :users, controllers: { sessions: 'sessions', :registrations => 'registrations', :omniauth_callbacks => 'omniauth_callbacks' }  do
    	get 'logout' => 'devise/sessions#destroy'
	end
	match '/users/:id/finish_signup' => 'users#finish_signup', via: [:get, :patch], :as => :finish_signup


	get '/about' => 'misc#about'
	get '/why' => 'misc#why'
	get '/terms-condition' => 'misc#terms-condition'
	get '/pledge-now' => 'misc#pledge_now'
	get '/how' => 'misc#how'
	match '/feedback' => 'misc#feedback', via: [:get, :post]
	get '/disclaimer' => 'misc#disclaimer'
	get '/terms-and-conditions' => 'misc#terms_and_conditions'

	get '/api/find_children' => 'api#find_children'
	post '/payment' => 'api#payment'
	post '/valid-refid' => 'api#valid_refid'
	post '/shared-contributions/:id/' => 'api#payment'

	get '/update-profile' => 'users#update_profile'

	post '/payu/ok' => 'payu#ok'
	post '/payu/error' => 'payu#error'
	post '/payu/report' => 'payu#report'

	post '/profile-update' => 'users#profile_update'
	get '/member-associate' => 'users#member_associate'
	match '/save-profile' => 'users#save_profile', via: [:patch, :post]
	post '/save-child' => 'users#save_child'

	get '/buddy-referrals' => 'users#buddy_referrals'
	get '/registered-children' => 'users#registered_children'
	post '/invite-referral' => 'users#invite_referral'
	match '/change-password' => 'users#change_password', via: [:get, :post]

	post 'payment/checkout'
  	get 'payment/:id/cancle' => 'payment#cancle'
	get 'payment/:id/success' => 'payment#success'
	post 'payment/:id/ipn_notify' => 'payment#ipn_notify'

end

class AddColumnToGuardians < ActiveRecord::Migration
  def change
    add_column :guardians, :appt_no, :string
    add_column :guardians, :st_no, :string
    add_column :guardians, :city, :string
  end
end

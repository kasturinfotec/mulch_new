class PaymentMailer < ActionMailer::Base

	def ipn_notify params, user, _session
		@params = params
		@user = user
		@contrib_session = _session
		mail(:to => "unitedsponsorsofamerica@gmail.com", :subject => "IPN Notification!", :from => "commulch@gmail.com")
	end

end

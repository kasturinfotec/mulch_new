include MiscHelper

class ApiController < ApplicationController
	
	# Find Children
	def find_children
		_query = []
		_q = []
		_query << "LOWER(first_nm) like '%#{params[:search].downcase}%'" if !params[:search].blank?
		_query << "LOWER(last_nm) like '%#{params[:search].downcase}%'" if !params[:search].blank?
		_query << "LOWER(ref_id) = '#{params[:search].downcase}'" if !params[:search].blank?
		_q << "role = 'children'" << "id NOT IN (#{current_user.id})"
		children = User.where(_query.join(" or ")).where(_q.join(" and ")) rescue 0
		render :json => { count: children.length, children: children.map { |children| {:id => children.id, :first_nm => children.first_nm.try(:titlecase), :last_nm => children.last_nm.try(:titlecase), :ref_id => children.ref_id, profile: "#{children.avatar.url rescue '/assets/photo_not_found.png'}", email: children.email} } }
	end
	
	# Paypal Integration for Coffee Contribution
	def payment
		pos = Payu::Pos.new :pos_id => Figaro.env.payu_pos_id, :pos_auth_key => Figaro.env.payu_pos_auth_key, :key1 => Figaro.env.payu_key1, :key2 => Figaro.env.payu_key2, :add_signature => Figaro.env.payu_add_signature
		@transaction = pos.new_transaction(:first_name => params[:x_first_name], :last_name => params[:x_last_name], :email => "vajapravin23@gmail.com", :client_ip => request.remote_ip, :amount => params[:total_contributions], desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.")
	end

	def valid_refid
		refferal = User.find_by_ref_id(params[:ref_id]) rescue nil
		if !refferal.nil?
			render json: { status: :success, refferal_id: refferal.id }
		else
			render json: { status: :error, message: 'Referral is not found!' }
		end
	end
end

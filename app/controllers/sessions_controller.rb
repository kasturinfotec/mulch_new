class SessionsController < Devise::SessionsController
	after_filter :clear_sign_signout_flash, :only => [:destroy, :new]

	def new
		session[:ref_id] = params[:ref_id]
		super
	end

	def create
		self.resource = resource_class.new(sign_in_params)
		user = User.find_by_email(params[:user][:email])
		if user
			if user && user.valid_password?(params[:user][:password])
				sign_in(:user, user)
				set_flash_message(:notice, :signed_in) if is_flashing_format?
				respond_with resource, location: after_sign_in_path_for(resource)
			else
				clean_up_passwords(resource)
				set_flash_message(:notice, :invalid) if is_flashing_format?
				render template: '/devise/sessions/new.html.erb'
			end
		else
			set_flash_message(:notice, :not_found) if is_flashing_format?
			render template: '/devise/sessions/new.html.erb'
		end
	end

	def logout
		sign_out(:user, current_user)
		redirect_to root_path
	end

  	protected
		def sign_in_params
			devise_parameter_sanitizer.sanitize(:sign_in)
		end

		def clear_sign_signout_flash
			flash.clear
		end
end

ActiveAdmin.register Donation do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #

   index do
    selectable_column
    id_column
    column :total_contribution
    column :updated_contribution
    column :frequency
    column :payment_method
    column :created_at
    actions
  end


  permit_params :total_contribution, :updated_contribution, :frequency, :payment_method ,:status ,  :created_at ,:updated_at
  
  filter :status
  filter :frequency
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end

# == Schema Information
#
# Table name: children_donators
#
#  id          :integer          not null, primary key
#  children_id :integer
#  donator_id  :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class ChildrenDonator < ActiveRecord::Base
	belongs_to :child, class_name: 'User', foreign_key: 'donator_id'
	belongs_to :donator, class_name: 'User', foreign_key: 'children_id'
end

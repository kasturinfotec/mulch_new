# == Schema Information
#
# Table name: donation_cups
#
#  id          :integer          not null, primary key
#  donation_id :integer
#  cup_id      :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class DonationCup < ActiveRecord::Base
	belongs_to :donation
	belongs_to :cup
end
